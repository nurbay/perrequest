import akka.actor.{ActorSystem, Props}
import akka.io.IO
import akka.util.Timeout
import com.typesafe.config.ConfigFactory
import kz.darlab.l1.{ChildActor, MainActor, RestRouting_}
import spray.can.Http

import scala.concurrent.duration._

object Boot  extends  App { //with SimpleRoutingApp {


  implicit val system = ActorSystem("Darlab")

  //val childActor =system.actorOf(ChildActor.props)
  //val mainActor = system.actorOf(MainActor.props(childActor))


  implicit val timeout = Timeout(3 seconds)

  val config = ConfigFactory.load ("local.conf")
  val host = config.getString( "service.host")
  val port = config.getInt( "service.port")

  val routeActor = system.actorOf(Props(new kz.darlab.l1.routing.RestRouting))
  IO(Http) ! Http.Bind (routeActor ,"localhost", port=8080)

  //def start = startServer(interface = host, port = port) {}
    //-DConfig.resource=local.conf
}


