package kz.darlab.l1


import akka.actor.{Actor, ActorContext, ActorLogging, ActorRef, DeadLetter, Props}
import kz.darlab.l1.ChildActor.Pong


object ChildActor  {
  case class Pong()
  def props :Props = Props[ChildActor]

}


class ChildActor  extends Actor with ActorLogging{
  val eventStream =  context.system.eventStream.subscribe(self, classOf[DeadLetter])
   def receive :Receive={
  //    case Pong => log.info ("Receive pong")
      case s:String => {
        log.info(s"Message from parent: $s")
       // sender() ! "your child"
      }
   //   case DeadLetter(a,b,c) => println("!!!!!!!!!")  //log.info ("d")

  }
}
