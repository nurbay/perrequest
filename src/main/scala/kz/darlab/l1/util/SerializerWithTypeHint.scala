package kz.darlab.l1.util

//import kz.darlab.akka.domain.serializers.PersonaSerializer
import kz.darlab.akka.domain.serializers.OnlineShopSerializer
import org.json4s.ShortTypeHints
import org.json4s.native.Serialization

trait SerializerWithTypeHint extends OnlineShopSerializer  {

  implicit val formats = Serialization.formats(
    ShortTypeHints(
      onlineShopTypeHints
    )
  )

}
