package kz.darlab.l1.routing


import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props}
import kz.darlab.akka.domain.entities.{ApiAkkaRequest, DomainAkka}
import kz.darlab.l1.{ChildActor, MainActor}
import kz.darlab.l1.routing.PerRequest.WithActorRef
import kz.darlab.l1.util.SerializerWithTypeHint
import spray.http.StatusCode
import spray.httpx.Json4sSupport
import spray.routing.RequestContext

trait PerRequest  extends  Actor with ActorLogging with Json4sSupport with SerializerWithTypeHint {

  val json4sFormats = formats

  def r: RequestContext
  def target: ActorRef
  def msg: ApiAkkaRequest

  target ! msg

  override def receive: Receive = {

    case res: DomainAkka => complete(StatusCode.int2StatusCode(200), res)
  }


  def complete[T<: AnyRef](value: StatusCode, akka: T): Unit = {
    r.complete(value, akka)
    context.stop(self)
  }

}

object PerRequest {
  case class WithActorRef(r: RequestContext,  msg: ApiAkkaRequest,prop:Props) extends PerRequest{
    def target = context.actorOf(prop)
  }
}

trait PerRequestInit {
  this: Actor =>

  def perRequest(r: RequestContext, msg: ApiAkkaRequest,props:Props) ={
    context.actorOf(Props(new WithActorRef(r,msg,props)))
  }
}