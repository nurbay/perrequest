package kz.darlab.l1.routing

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import kz.darlab.akka.domain.entities.{ApiAkkaRequest, Category, Customer}
import kz.darlab.l1.MainActor
import kz.darlab.l1.MainActor.HandlePingRequest
import kz.darlab.l1.util.SerializerWithTypeHint
import org.json4s.DefaultFormats
import spray.httpx.Json4sSupport
import spray.routing.{HttpService, Route}

import scala.concurrent.ExecutionContext


class RestRouting extends HttpService  with Actor with Json4sSupport
  with SerializerWithTypeHint  with PerRequestInit with ActorLogging{

  implicit val executionContext: ExecutionContext = context.dispatcher
  implicit def actorRefFactory = context

  override def receive: Receive = runRoute(route)
  implicit val json4sFormats = formats


  val route = pathPrefix("akka") {
    post {
      path("post" ){
        entity(as[Category]) { person =>
          handleRequest(HandlePingRequest(person),MainActor.props)
        }
      }
    }
  }

  def handleRequest(msg: ApiAkkaRequest,props:Props): Route = {
    ctx => perRequest(ctx, msg,props)
  }
}


