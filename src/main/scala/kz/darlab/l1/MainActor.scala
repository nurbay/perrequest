package kz.darlab.l1
import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.util.Timeout
import akka.pattern._
import kz.darlab.akka.domain.entities.{ApiAkkaRequest, Category, Customer}
import kz.darlab.l1.ChildActor.Pong

import scala.concurrent.duration._
import kz.darlab.l1.MainActor.{HandlePingRequest, Ping}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}


object MainActor {
  case class Ping()
  case class HandlePingRequest(person: Category) extends ApiAkkaRequest

  def props:Props = Props(new MainActor)
}


class MainActor   extends Actor with ActorLogging {
  implicit  val tout = Timeout (100 seconds)

  def receive: Receive = {
    case HandlePingRequest(person) =>
     // log.debug(s"Person is : $person")
   // sender() ! person.copy(name = "RESPONSE")
    {
      log.info("this is mainactor")
      context.parent ! person.copy(name = "RESPONSE")
    }

    case Ping =>
      log.info(s"Received Ping")

  }





//bitbucket
  }

/*
 val some = Future {
    val child = context.actorOf(Props[ChildActor], "my_child")

    val future = child ? "1"
    val result = Await.result(future, tout.duration).asInstanceOf[String]
    println(result)
  }
 */

